# **WE HAVE MOVED**

The development and maintenance of this repository has moved to [**github**](https://github.com/ethz-adrl/control-toolbox). 

Please visit

 * https://github.com/ethz-adrl/control-toolbox for for accessing a **maintained release**, making contributions, and posting issues.

 * https://ethz-adrl.github.io/ct/ for up-to-date **documentation**


Here, on bitbucket, legacy releases up to ct v2.3 will be kept, however they are supported any more.